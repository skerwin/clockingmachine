<?php


date_default_timezone_set("Europe/London");
$date = date('H:i:s', time());


?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
        <link rel="stylesheet" href="style.css">
        <script type="text/javascript" src="http://jqueryjs.googlecode.com/files/jquery-1.3.1.min.js" > </script>
        <script src="csv.js" type="text/javascript" charset="utf-8"></script>
        <script>
            $(function(){
                $("#export").click(function(){
                    $("#export_table").tableToCSV();
                });
            });
        </script>
    </head>
  	<body>
        <div class="container">

            <?php
            $servername = "localhost";
            $username = "root";
            $password = "root";
            $dbname = "mydb";

            // Create connection
            $conn = mysqli_connect($servername, $username, $password, $dbname);
            // Check connection
            if (!$conn) {
                die("Connection failed: " . mysqli_connect_error());
            }

            $sql = "SELECT * FROM users";
            $result = mysqli_query($conn, $sql);

            $sql2 = "SELECT * FROM departments";
            $result2 = mysqli_query($conn, $sql2);

            ?>


            <br><br><br>
            <form method="post" action="<?php $_SERVER['PHP_SELF'] ?>">


            <div class="row">
                <div class="col-md-6">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Department
                        </div>
                        <div class="panel-body">
                            <select class="form-control" name="employeeName">
                                <option value="all">All Departments</option>
                                <?php if (mysqli_num_rows($result2) > 0) {
                                    while($row = mysqli_fetch_assoc($result2)) {
                                    $dept_id = $row['department_id'];
                                    $dept_name = $row['department_name'];
                                    ?>
                                    <option value="<?php echo $dept_id; ?>"><?php echo $dept_name; ?></option>
                                <?php } } ?>

                            </select>
                        </div>
                    </div><!-- end panel -->
                </div><!-- end 6 -->

                <div class="col-md-6">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Employee Name
                        </div>
                        <div class="panel-body">
                            <select class="form-control" name="employeeName">
                                <option value="all">All Employees</option>
                                <?php if (mysqli_num_rows($result) > 0) {
                                    while($row = mysqli_fetch_assoc($result)) {
                                    $u_id = $row['id'];
                                    $usrname = $row['first_name'] . " " . $row['last_name'];
                                    ?>
                                    <option value="<?php echo $u_id; ?>"><?php echo $usrname; ?></option>
                                <?php } } ?>

                            </select>
                        </div>
                    </div><!-- end panel -->
                </div><!-- end 6 -->




        <div class="row">
            <div class="col-md-12">
                <div class="col-md-6">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            From
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-4">
                                    <label>Day</label>
                                    <select type="text" class="form-control" id="fromCalDay" name="fromCalDay">
                                        <?php for($i = 1; $i <= 31; $i++) { ?>
                                                <option value="<?php echo $i; ?>" <?php if ($_POST['fromCalDay'] == $i) echo 'selected="selected"'; ?>  ><?php echo $i; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>

                                <div class="col-md-4">
                                    <label>Month</label>
                                    <select type="text" class="form-control" id="fromCalMonth" name="fromCalMonth">
                                        <option <?php if ($_POST['fromCalMonth'] == '01') echo 'selected="selected"'; ?> value="01">January</option>
                                        <option <?php if ($_POST['fromCalMonth'] == '02') echo 'selected="selected"'; ?> value="02">February</option>
                                        <option <?php if ($_POST['fromCalMonth'] == '03') echo 'selected="selected"'; ?> value="03">March</option>
                                        <option <?php if ($_POST['fromCalMonth'] == '04') echo 'selected="selected"'; ?> value="04">April</option>
                                        <option <?php if ($_POST['fromCalMonth'] == '05') echo 'selected="selected"'; ?> value="05">May</option>
                                        <option <?php if ($_POST['fromCalMonth'] == '06') echo 'selected="selected"'; ?> value="06">June</option>
                                        <option <?php if ($_POST['fromCalMonth'] == '07') echo 'selected="selected"'; ?> value="07">July</option>
                                        <option <?php if ($_POST['fromCalMonth'] == '08') echo 'selected="selected"'; ?> value="08">August</option>
                                        <option <?php if ($_POST['fromCalMonth'] == '09') echo 'selected="selected"'; ?> value="09">September</option>
                                        <option <?php if ($_POST['fromCalMonth'] == '10') echo 'selected="selected"'; ?> value="10">October</option>
                                        <option <?php if ($_POST['fromCalMonth'] == '11') echo 'selected="selected"'; ?> value="11">November</option>
                                        <option <?php if ($_POST['fromCalMonth'] == '12') echo 'selected="selected"'; ?> value="12">December</option>
                                    </select>
                                </div>
                                <div class="col-md-4">
                                    <label>Year</label>
                                    <select type="text" class="form-control" id="fromCalYear" name="fromCalYear">
                                        <option <?php if ($_POST['fromCalYear'] == '2016') echo 'selected="selected"'; ?> value="2016">2016</option>
                                        <option <?php if ($_POST['fromCalYear'] == '2017') echo 'selected="selected"'; ?> value="2017">2017</option>
                                        <option <?php if ($_POST['fromCalYear'] == '2018') echo 'selected="selected"'; ?> value="2018">2018</option>
                                    </select>
                                </div>
                            </div><!-- end row -->
                        </div><!-- end panel body -->
                    </div><!-- end panel -->
                </div>
                <div class="col-md-6">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            To
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-4">
                                    <label>Day</label>
                                    <select type="text" class="form-control" id="toCalDay" name="toCalDay">
                                        <?php for($i = 1; $i <= 31; $i++) { ?>
                                                <option value="<?php echo $i; ?>" <?php if ($_POST['toCalDay'] == $i) echo 'selected="selected"'; ?>  ><?php echo $i; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>

                                <div class="col-md-4">
                                    <label>Month</label>
                                    <select type="text" class="form-control" id="toCalMonth" name="toCalMonth">
                                        <option <?php if ($_POST['toCalMonth'] == '01') echo 'selected="selected"'; ?> value="01">January</option>
                                        <option <?php if ($_POST['toCalMonth'] == '02') echo 'selected="selected"'; ?> value="02">February</option>
                                        <option <?php if ($_POST['toCalMonth'] == '03') echo 'selected="selected"'; ?> value="03">March</option>
                                        <option <?php if ($_POST['toCalMonth'] == '04') echo 'selected="selected"'; ?> value="04">April</option>
                                        <option <?php if ($_POST['toCalMonth'] == '05') echo 'selected="selected"'; ?> value="05">May</option>
                                        <option <?php if ($_POST['toCalMonth'] == '06') echo 'selected="selected"'; ?> value="06">June</option>
                                        <option <?php if ($_POST['toCalMonth'] == '07') echo 'selected="selected"'; ?> value="07">July</option>
                                        <option <?php if ($_POST['toCalMonth'] == '08') echo 'selected="selected"'; ?> value="08">August</option>
                                        <option <?php if ($_POST['toCalMonth'] == '09') echo 'selected="selected"'; ?> value="09">September</option>
                                        <option <?php if ($_POST['toCalMonth'] == '10') echo 'selected="selected"'; ?> value="10">October</option>
                                        <option <?php if ($_POST['toCalMonth'] == '11') echo 'selected="selected"'; ?> value="11">November</option>
                                        <option <?php if ($_POST['toCalMonth'] == '12') echo 'selected="selected"'; ?> value="12">December</option>
                                    </select>
                                </div>
                                <div class="col-md-4">
                                    <label>Year</label>
                                    <select type="text" class="form-control" id="toCalYear" name="toCalYear">
                                        <option <?php if ($_POST['toCalYear'] == '2016') echo 'selected="selected"'; ?> value="2016">2016</option>
                                        <option <?php if ($_POST['toCalYear'] == '2017') echo 'selected="selected"'; ?> value="2017">2017</option>
                                        <option <?php if ($_POST['toCalYear'] == '2018') echo 'selected="selected"'; ?> value="2018">2018</option>
                                    </select>
                                </div>
                            </div><!-- end row -->
                        </div><!-- end panel body -->
                    </div><!-- end panel -->
                </div>
            </div>
        </div>

                    <div class="col-md-12">
                        <input type="submit" class="btn btn-success" value="Filter" name="filter" id="filter">
                        <span  class="btn btn-warning" id="export" data-export="export">Export as CSV</button>
                    </div>


            </div><!-- end outer row -->

            </form><!-- end form -->
            <br><br>


            <?php
            if (isset($_POST['filter'])) {

                $fromDay    = $_POST['fromCalDay'];
                $fromMonth  = $_POST['fromCalMonth'];
                $fromYear   = $_POST['fromCalYear'];
                $toDay      = $_POST['toCalDay'];
                $toMonth    = $_POST['toCalMonth'];
                $toYear     = $_POST['toCalYear'];
                $fromDate   = $fromYear . "-" . $fromMonth . "-" . $fromDay . " 00:00:00";
                $toDate     = $toYear . "-" . $toMonth . "-" . $toDay . " 00:00:00";

                if($_POST['employeeName'] != 'all') {
                    $employee   = $_POST['employeeName'];
                    $sql = "SELECT * FROM events INNER JOIN users ON events.user_id=users.id INNER JOIN departments ON users.department=departments.department_id WHERE `user_id` = '$employee' AND `start_date` BETWEEN '$fromDate' AND '$toDate' ";
                } else {
                    $sql = "SELECT * FROM events INNER JOIN users ON events.user_id=users.id INNER JOIN departments ON users.department=departments.department_id WHERE `start_date` BETWEEN '$fromDate' AND '$toDate' ";
                }

            //$sql = "SELECT * FROM events INNER JOIN users ON events.user_id=users.id WHERE `start_date` BETWEEN '$fromDate' AND '$toDate' ";
            //echo $sql;

            $result = mysqli_query($conn, $sql);


            ?>


            <table class="table table-bordered table-striped table-responsive" id="export_table">
                <thead>
                    <td>Employee ID</td> <td>Employee Name</td><td>Department</td> <td>Start Date/Time</td><td>End Date/Time</td><td>Time Worked</td>
                </thead>
            <?php //start loop






            if (mysqli_num_rows($result) > 0) {
                // output data of each row
                while($row = mysqli_fetch_assoc($result)) {
                    $id = $row["id"];
                    $user_id = $row["user_id"];
                    $startDate = $row["start_date"];
                    $department = $row["department_name"];
                    $endDate = "";
                    if (!empty($row["end_date"])) {
                            $endDate = $row['end_date'];
                    }
                    $eventType = $row["event_types_id"];
                    $employeeName = $row["first_name"] . " " . $row["last_name"];
                     ?>

                    <tr>
                        <td><?php echo $user_id; ?></td>
                        <td><?php echo $employeeName; ?></td>
                        <td><?php echo $department; ?></td>
                        <td><?php echo $startDate; ?></td>
                        <td><?php echo $endDate; ?></td>
                        <td><?php $start = DateTime::createFromFormat('Y-m-d H:i:s', $startDate);
                        if (!empty($row["end_date"])) {
                                $endDate = $row['end_date'];
                                $end = DateTime::createFromFormat('Y-m-d H:i:s', $endDate);
                                $worktime = $start->diff($end);
                                $hours = (strtotime($endDate) - strtotime($startDate)) / 3600;
                                echo $worktime->format('%h'. " hours " . '%i' . " mins"); } else { ?><span class="red-overtime">Didn't Clock Out!</span><?php } ?></td>
                    </tr>
            <?php }
            } else {
                echo "0 results";
            }
            mysqli_close($conn);
            ?>

            </table>

            <?php } ?>


	    </div>

        <?php include 'footer.php'; ?>
    </body>
</html>
