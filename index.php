<?php

$servername = "localhost";
$username = "root";
$password = "root";
$dbname = "mydb";

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}


date_default_timezone_set("Europe/London");
$date = date('Y-m-d H:i:s', time());


?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
 	<link rel="stylesheet" href="style.css">
    </head>

 
				 
  <script>
	function startTime() {
	    var today = new Date();
	    var h = today.getHours();
	    var m = today.getMinutes();
	    var s = today.getSeconds();
	    m = checkTime(m);
	    s = checkTime(s);
	    document.getElementById('txt').innerHTML =
	    h + ":" + m + ":" + s;
	    var t = setTimeout(startTime, 500);
	}
	function checkTime(i) {
	    if (i < 10) {i = "0" + i};  // add zero in front of numbers < 10
	    return i;
	}




</script>


  <body onload="startTime()">

 <div class="outer">
	<div class="middle">
		<div class="inner">

    	<!-- if login button pressed  -->
    	<?php if(isset($_POST['login'])) {
			
			$username = $_POST['username'];
    	 	$password = $_POST['password'];
    	 	$token = $_POST['token'];

    	 	 

    	 	if (!empty($token)) {
	    	 	// $sql = "SELECT * FROM `users` WHERE `username` = '$username' and `password` = '$password' OR `token` = '$token'" ;
	    	  	$sql = "SELECT * FROM `users` WHERE `token` = '$token'" ;
	    	 	$result = $conn->query($sql);

	    	 	 

	    	 	if ($result->num_rows > 0) {
				    while($row = $result->fetch_assoc()) {
				       $user_id = $row['id'];
				       $username = $row['username'];
				       $token = $row['token'];
				       
				    } // end while

				    // Now check the last log event
			    	$sql = "SELECT * FROM `events` where `user_id` = $user_id order by start_date desc limit 1";
			    	$result = $conn->query($sql);
					    if ($result->num_rows > 0) {
						    while($row = $result->fetch_assoc()) {
						       $event_id = $row['event_types_id'];
						    }
						} // end if


						// If the last event was loging out, then let them login
						if ($event_id == 0 || $event_id == 2) {
 							$sql = "INSERT INTO `events`(`user_id`, `start_date`,`event_types_id`,`users_id`) VALUES ('$user_id','$date', 1 ,'$user_id')";
							if ($conn->query($sql) === TRUE) { ?>
								
								<div class="clockMessage ClockedIn">
									 <div class="outer">
										<div class="middle">
											<div class="inner text-center">
												Success!<br><b><?php echo $username; ?></b><br>Clocked In.
											</div>
										</div>
									</div>
								</div>


							<?php
							} else {
							    echo "Error: " . $sql . "<br>" . $conn->error;
							}
						} elseif($event_id == 1) { // if last login event was a 1 = clock in, then let user clock out
  							$sql = "UPDATE `events` SET `end_date` = ('$date'), `event_types_id` = 2 WHERE `user_id` = $user_id ORDER BY `id` DESC LIMIT 1";
 							if ($conn->query($sql) === TRUE) { ?>
 								
 								<div class="clockMessage ClockedOut">
									 <div class="outer">
										<div class="middle">
											<div class="inner text-center">
												Success!<br><b><?php echo $username; ?></b><br>Clocked Out.
											</div>
										</div>
									</div>
								</div>

 							<?php
 							} else {
 							    echo "Error: " . $sql . "<br>" . $conn->error;
 							}
                         }

						$conn->close();

						}
					}
				} ?>


						 

						<img src="logo.svg" class="logo">

					 	<form method="post" action="<?php $_SERVER['PHP_SELF'] ?>">
					 		<input type="password" name="token" id="token" placeholder="Barcode" class="form-control input-lg" disabled autofocus><br>
				    		<input type="text" name="username" id="username" placeholder="username" class="form-control input-lg hidden"><br>
				    		<input type="password" name="password" id="password" placeholder="password" class="form-control input-lg hidden">
							<input type="submit" name="login" id="login" value="Clock In" class="hidden">
			    	    </form><!-- end form -->

				    	<span class="clock">Current Time: <div id="txt"></div></span>

						</div>
					</div>
				</div>
				<script
			  src="https://code.jquery.com/jquery-3.1.0.min.js"
			  integrity="sha256-cCueBR6CsyA4/9szpPfrX3s49M9vUU5BgtiJj06wt/s="
			  crossorigin="anonymous"></script>

			 	<script type="text/javascript">

			 	$(document).ready(function(){
					setTimeout(function(){
						$('#token').removeAttr("disabled");
						$("#token").focus();
						$('.clockMessage').addClass('hidden');

						}, 1500);
					});

			 	$(document).ready(function(){
					 
					});

			 	</script>


                <?php include 'footer.php'; ?>

                <script
			  src="https://code.jquery.com/jquery-3.1.0.min.js"
			  integrity="sha256-cCueBR6CsyA4/9szpPfrX3s49M9vUU5BgtiJj06wt/s="
			  crossorigin="anonymous"></script>



  </body>
</html>
